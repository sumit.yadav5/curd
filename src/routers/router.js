const express = require("express");
const users = require("../models/users");
const router = new express.Router();
router.post("/users", async (req, res) => {
    try{
     const userRecodes =  new users(req.body);
     const userResponse =  await userRecodes.save();
     res.status(201).send(userResponse);
    }catch(error){
       res.status(400).send(error);
    }
});

router.get("/users", async (req, res) => {
    try{
       const getUsers = await users.find({});
       res.send(getUsers);
    }catch(error){
       res.status(400).send(error);
    }
});

router.get("/users/:id", async (req, res) => {
    try{
        const _id = req.params.id;
        const getUser = await users.findById(_id);
        res.send(getUser);
    }catch(error){
       res.status(400).send(error)
    }
});

router.patch("/users/:id", async (req, res) => {
    try{
        const _id = req.params.id;
        const updateUser = await users.findByIdAndUpdate(_id, req.body,{
            new:true
        });
        res.send(updateUser);
    }catch(error){
       res.status(500).send(error)
    }
});

router.delete("/users/:id", async (req, res) => {
    try{
        const _id = req.params.id;
        const updateUser = await users.findByIdAndDelete(_id);
        res.send(updateUser);
    }catch(error){
       res.status(500).send(error)
    }
});

module.exports = router;