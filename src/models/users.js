const express = require("express");
const  mongoose  = require("mongoose");
const userSchema = new  mongoose.Schema({
    id:{
        type:Number,
        required:true,
        trim:true
    },
    name:{
        type:String,
        required:true,
        trim:true
    },
    userName:{
        type:String,
        required:true,
        trim:true
    },
    email:{
        type:String,
        required:true,
        trim:true
    }
})
const users = new mongoose.model("User",userSchema)
module.exports = users